//[1]count fruits on sale

db.fruits.aggregate([
{$match: {onSale: true}}, {$count: "fruitsOnSale"}
]);

//[2] count and count the total number of fruits with stock more than 20

db.fruits.aggregate([
  {$match: {stock: {$gte: 20}}}, 
  {$count: "enoughStock"}
  ]);

//[3] $average

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", avg_price: {$avg: "$price"}}},
  {$sort: {avg_price: -1}}
  ]);

//[4] $max

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", max_price: {$max: "$price"}}},
  {$sort: {max_price: 1}}
  ]);


//[5] $min

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$supplier_id", min_price: {$min: "$price"}}},
  {$sort: {min_price: 1}}
  ]);